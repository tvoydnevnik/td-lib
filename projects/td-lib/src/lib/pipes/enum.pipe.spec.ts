import { EnumPipe } from "./enum.pipe";

enum TestingEnum_String {
	TEST_1 = "x",
	TEST_2 = "y",
	TEST_3 = "z",
}

enum TestingEnum_Number {
	TEST_1 = 1,
	TEST_2 = 2,
	TEST_3 = 3,
}

describe("EnumPipe", () => {
	const enum_names = ["TEST_1", "TEST_2", "TEST_3"];
	let pipe: EnumPipe<any>;

	beforeEach(() => {
		pipe = new EnumPipe<any>();
	});

	it("should create an instance", () => {
		const pipe = new EnumPipe();
		expect(pipe).toBeTruthy();
	});

	it("should return object with enum names and values (string values)", function () {
		const values = ["x", "y", "z"];
		const res = pipe.transform(TestingEnum_String);

		res.forEach((o, i) => {
			expect(o.k).toBe(enum_names.at(i)!);
			expect(o.v).toEqual(values.at(i)!);
		});
	});

	it("should return object with enum names and values (number values)", function () {
		const values = [1, 2, 3];
		const res = pipe.transform(TestingEnum_Number);

		res.forEach((o, i) => {
			expect(o.k).toBe(enum_names.at(i)!);
			expect(o.v).toEqual(values.at(i)!);
		});
	});
});
