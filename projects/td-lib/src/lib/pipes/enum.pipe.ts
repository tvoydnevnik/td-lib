import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
	standalone: true,
	name: "enum",
})
export class EnumPipe<V> implements PipeTransform {
	transform(value: Record<string, V>): { k: string; v: V }[] {
		return Object.keys(value)
			.filter((k) => isNaN(+k))
			.map((k) => ({ k: k, v: value[k] }));
	}
}
