import { Pipe, PipeTransform } from "@angular/core";
import { DateTime } from "luxon";

@Pipe({
	standalone: true,
	name: "time",
})
export class TimePipe implements PipeTransform {
	transform(value: string, format?: string): string {
		const time = DateTime.fromISO(value);
		if (!time.isValid) return "–";

		return format
			? time.toFormat(format)
			: // this format becomes "HH:mm"
			  time.toISOTime({
					suppressSeconds: true,
					suppressMilliseconds: true,
					includeOffset: false,
			  }) ?? "";
	}
}
