import { TestBed } from "@angular/core/testing";
import { TimePipe } from "./time.pipe";
import { DateTime } from "luxon";

describe("TimePipe", function () {
	let pipe: TimePipe;
	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [TimePipe],
			providers: [TimePipe],
		});
		pipe = TestBed.inject(TimePipe);
	});

	it("should be created", function () {
		expect(pipe).toBeDefined();
	});

	it("should transform without arguments", function () {
		const source = DateTime.now();
		const v = source.toISOTime({ format: "extended" })!;

		expect(pipe.transform(v)).toBe(
			source.toISOTime({
				suppressSeconds: true,
				suppressMilliseconds: true,
				includeOffset: false,
			})!,
		);
	});

	it("should transform to format", function () {
		const source = DateTime.now();
		const format = "HH:mm";
		const v = source.toISOTime()!;

		expect(pipe.transform(v, format)).toBe(source.toFormat(format));
	});
});
