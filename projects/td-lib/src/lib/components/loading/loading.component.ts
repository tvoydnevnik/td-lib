import { Component } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";

@Component({
	standalone: true,
	imports: [CommonModule, MatProgressSpinnerModule],
	selector: "td-loading",
	templateUrl: "./loading.component.html",
	styleUrls: ["./loading.component.scss"],
})
export class LoadingComponent {}
