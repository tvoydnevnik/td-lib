import { MatPaginator } from "@angular/material/paginator";
import { ChangeDetectorRef } from "@angular/core";
import { ReplaySubject } from "rxjs";

export class ItemsListDatasource<T> {
	private _filteredDataStore: Array<T>;
	private _initialDataStore: Array<T>;
	public data$ = new ReplaySubject<Array<T>>(1);

	private _currentPageIndex: number = -1;
	private _currentPageSize: number = -1;
	private _currentSearch: string = "";

	/**
	 *
	 * @param cdr ChangeDetectorRef to deal with NG0100 errors on `this.data$` changes.
	 * @param data Optional data array
	 * @param paginator Optional paginator to control the list (should be set later)
	 */
	constructor(
		private readonly cdr: ChangeDetectorRef,
		private readonly data?: Array<T>,
		private paginator?: MatPaginator,
	) {
		this._initialDataStore = data || [];
		this._filteredDataStore = data || [];

		if (this.paginator) this._SetupPaginator();
	}

	Count(): number {
		return this._filteredDataStore.length;
	}

	UpdateData(value?: Array<T>): void {
		this._initialDataStore = value || [];
		this._filteredDataStore = value || [];
		this.SearchItems(this._currentSearch);
	}

	PaginateData(page: number, size: number): void {
		this._currentPageIndex = page;
		this._currentPageSize = size;
		if (this.paginator) {
			this.paginator.pageSize = this._currentPageSize;
			this.paginator.pageIndex = this._currentPageIndex;
		}
		this.data$.next(
			this._filteredDataStore.slice(page * size, page * size + size),
		);
		this.cdr.detectChanges();
	}

	SearchItems(search: string): void {
		this._currentSearch = search;
		this._filteredDataStore = this._initialDataStore.filter((item) =>
			JSON.stringify(item).toLowerCase().includes(search.toLowerCase()),
		);

		this.PaginateData(0, this._currentPageSize);
	}

	SetPaginator(paginator: MatPaginator): void {
		this.paginator = paginator;
		this._SetupPaginator();
	}

	private _SetupPaginator(): void {
		if (!this.paginator) return;

		this._currentPageIndex = this.paginator.pageIndex;
		this._currentPageSize = this.paginator.pageSize;
		this.PaginateData(this._currentPageIndex, this._currentPageSize);
	}
}
