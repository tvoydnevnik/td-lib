import { TransformOptions } from "../pipes/transform-options.type";

export class ItemProps {
	public options: ItemPropOptions[];

	constructor(private readonly mapping: ItemPropOptions[]) {
		this.options = mapping;
	}

	GetValue(
		target: any,
		keyOrLocation?: string | ItemPropLocation | ItemPropLocation[],
	): any {
		switch (typeof keyOrLocation) {
			case "string":
				return this.GetValueByKey(target, keyOrLocation);
			case "object":
				return this.GetNestedValueByLocation(target, keyOrLocation);
			default:
				return undefined;
		}
	}

	GetValueByKey(target: any, key: string): any {
		return target[key];
	}

	GetNestedValueByLocation(
		target: any,
		location: ItemPropLocation | ItemPropLocation[],
	): any {
		if (!target) return target;

		if (!Array.isArray(location)) return target[location.key];

		const current_location = location[0];
		if (location.length === 0) return target;

		if (location.length === 1) return target[current_location.key];

		if (current_location.isArray)
			return (target[current_location.key] as Array<any>)?.flatMap((value) =>
				this.GetNestedValueByLocation(value, location.slice(1)),
			);

		const sliced_target = target[current_location.key];
		return this.GetNestedValueByLocation(sliced_target, location.slice(1));
	}
}

export interface ItemPropOptions {
	title: string;
	key?: string;
	location?: ItemPropLocation | ItemPropLocation[];
	transform?: TransformOptions[];
}

export interface ItemPropLocation {
	key: string;
	isArray?: boolean;
}
