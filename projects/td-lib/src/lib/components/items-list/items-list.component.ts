import { ItemEntryDescriptionDirective } from "./directives/item-entry-description.directive";
import { ItemEntryContentDirective } from "./directives/item-entry-content.directive";
import { ItemEntryTitleDirective } from "./directives/item-entry-title.directive";
import { ItemsListDatasource } from "./interfaces/items-list-datasource.type";
import { TransformByOptionsPipe } from "./pipes/transform-by-options.pipe";
import { MaterialImportsModule } from "../../material-imports.module";
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import { TdLoadDirective } from "../../directives/td-load.directive";
import { TdIfDirective } from "../../directives/td-if.directive";
import { ItemProps } from "./interfaces/item-columns.type";
import { BreakpointObserver } from "@angular/cdk/layout";
import { CommonModule } from "@angular/common";
import { FormControl } from "@angular/forms";
import {
	AfterViewInit,
	ChangeDetectorRef,
	Component,
	ContentChild,
	ContentChildren,
	Input,
	QueryList,
	ViewChild,
} from "@angular/core";

@Component({
	selector: "td-items-list",
	standalone: true,
	imports: [
		CommonModule,
		MaterialImportsModule,
		TransformByOptionsPipe,
		TdIfDirective,
		TdLoadDirective,
	],
	templateUrl: "./items-list.component.html",
	styleUrls: ["./items-list.component.scss"],
})
export class ItemsListComponent<T> implements AfterViewInit {
	@ViewChild(MatPaginator) paginator!: MatPaginator;

	@ContentChild(ItemEntryTitleDirective)
	itemEntryTitle?: ItemEntryTitleDirective;
	@ContentChild(ItemEntryDescriptionDirective)
	itemEntryDescription?: ItemEntryDescriptionDirective;
	@ContentChildren(ItemEntryContentDirective)
	itemEntryContentNodes?: QueryList<ItemEntryContentDirective>;

	@Input() dataSource?: ItemsListDatasource<T>;
	@Input() itemProps?: ItemProps;

	searchControl = new FormControl<string>("", { nonNullable: true });
	smallScreen = false;

	constructor(
		private readonly changeRef: ChangeDetectorRef,
		private readonly bpo: BreakpointObserver,
	) {
		this.bpo.observe("(max-width: 620px)").subscribe({
			next: (state) => {
				this.smallScreen = state.matches;
			},
		});
	}

	ngAfterViewInit() {
		this.dataSource?.SetPaginator(this.paginator);
		this.searchControl.valueChanges.subscribe({
			next: (value) => this.dataSource?.SearchItems(value),
		});

		this.changeRef.detectChanges();
	}

	onPage(evt: PageEvent): void {
		this.dataSource?.PaginateData(evt.pageIndex, evt.pageSize);
		this.changeRef.detectChanges();
	}
}
