import { PipeTransform } from "@angular/core";

type ExtractNonValueArguments<T extends PipeTransform> = Parameters<
	T["transform"]
> extends [value: string, ...rest: infer RestParams]
	? RestParams
	: never;

export interface TransformOptions<T extends PipeTransform = PipeTransform> {
	pipe: { new (): T } | T;
	arguments?: ExtractNonValueArguments<T>;
}
