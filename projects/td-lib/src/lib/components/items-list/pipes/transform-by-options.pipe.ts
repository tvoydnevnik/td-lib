import { Pipe, PipeTransform } from "@angular/core";
import { TransformOptions } from "./transform-options.type";

@Pipe({
	standalone: true,
	name: "tdTransformByOptions",
})
export class TransformByOptionsPipe implements PipeTransform {
	transform(value: string, options?: TransformOptions[]): string | null {
		if (!options || options.length === 0) return value;

		let transformedValue: string | null = value;
		for (let option of options) {
			// Instantiate new pipe instance
			let pipe = this._constructPipe(option);

			if (transformedValue !== null)
				transformedValue = pipe.transform(
					transformedValue,
					...(option.arguments || []),
				);
		}

		return transformedValue;
	}

	private _constructPipe(options: TransformOptions): PipeTransform {
		let pipe: PipeTransform;

		if (typeof options.pipe === "object") pipe = options.pipe;
		else pipe = new options.pipe();

		return pipe;
	}
}
