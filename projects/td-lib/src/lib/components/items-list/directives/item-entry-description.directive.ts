import { Directive, TemplateRef } from "@angular/core";
import { ItemsListContext } from "../interfaces/items-list.context";

@Directive({
	standalone: true,
	selector: "[tdItemDescription]",
})
export class ItemEntryDescriptionDirective {
	constructor(public templateRef: TemplateRef<ItemsListContext<any>>) {}

	static ngTemplateContextGuard(
		directive: ItemEntryDescriptionDirective,
		context: unknown,
	): context is ItemsListContext<any> {
		return true;
	}
}
