import { Directive, TemplateRef } from "@angular/core";
import { ItemsListContext } from "../interfaces/items-list.context";

@Directive({
	standalone: true,
	selector: "[tdItemTitle]",
})
export class ItemEntryTitleDirective {
	constructor(public templateRef: TemplateRef<ItemsListContext<any>>) {}

	static ngTemplateContextGuard(
		directive: ItemEntryTitleDirective,
		context: unknown,
	): context is ItemsListContext<any> {
		return true;
	}
}
