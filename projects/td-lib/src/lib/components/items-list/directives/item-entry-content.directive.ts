import { Directive, TemplateRef } from "@angular/core";
import { ItemsListContext } from "../interfaces/items-list.context";

@Directive({
	standalone: true,
	selector: "[tdItemContent]",
})
export class ItemEntryContentDirective {
	constructor(public templateRef: TemplateRef<ItemsListContext<any>>) {}

	static ngTemplateContextGuard(
		directive: ItemEntryContentDirective,
		context: unknown,
	): context is ItemsListContext<any> {
		return true;
	}
}
