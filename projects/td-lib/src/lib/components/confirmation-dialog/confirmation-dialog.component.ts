import { Component, Inject } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MAT_DIALOG_DATA, MatDialogModule } from "@angular/material/dialog";
import { ConfirmationDialogData } from "./confirmation-dialog-data.interface";
import { MatButtonModule } from "@angular/material/button";

@Component({
	selector: "td-confirmation-dialog",
	standalone: true,
	imports: [CommonModule, MatDialogModule, MatButtonModule],
	templateUrl: "./confirmation-dialog.component.html",
})
export class ConfirmationDialogComponent {
	constructor(
		@Inject(MAT_DIALOG_DATA)
		public readonly dialogData: ConfirmationDialogData,
	) {}
}
