export interface ConfirmationDialogData {
	title: string;
	warning?: string;
}
