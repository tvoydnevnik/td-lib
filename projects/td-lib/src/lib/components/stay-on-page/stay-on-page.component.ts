import { MaterialImportsModule } from "../../material-imports.module";
import { CommonModule } from "@angular/common";
import { Subscription } from "rxjs";
import {
	AfterViewInit,
	Component,
	EventEmitter,
	Input,
	OnDestroy,
	Optional,
	Output,
} from "@angular/core";
import {
	ControlContainer,
	FormControl,
	FormGroup,
	ReactiveFormsModule,
} from "@angular/forms";

@Component({
	selector: "td-stay-on-page",
	standalone: true,
	imports: [CommonModule, MaterialImportsModule, ReactiveFormsModule],
	templateUrl: "./stay-on-page.component.html",
	styleUrls: ["./stay-on-page.component.scss"],
})
export class StayOnPageComponent implements AfterViewInit, OnDestroy {
	@Input() controlName?: string;
	@Input() tooltip?: string;

	@Output() toggled = new EventEmitter<{ toggled: boolean }>();

	toggle = new FormControl<boolean>(false);

	private _sub$ = new Subscription();

	constructor(
		@Optional() private readonly controlContainer: ControlContainer,
	) {}

	ngAfterViewInit() {
		if (
			this.controlContainer &&
			this.controlContainer.control instanceof FormGroup
		) {
			this.controlContainer.control.setControl(
				this.controlName ? this.controlName : "stayOnPage",
				this.toggle,
			);
		}

		this._sub$.add(
			this.toggle.valueChanges.subscribe((v) =>
				this.toggled.emit({ toggled: !!v }),
			),
		);
	}

	ngOnDestroy() {
		this._sub$.unsubscribe();
	}
}
