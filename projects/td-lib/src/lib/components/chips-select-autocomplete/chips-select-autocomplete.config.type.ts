import { ItemDisplayFunction } from "../../interfaces/item-list-functions.type";
import {
	BaseItemsSelectionConfig,
	ItemsSelectionConfig,
} from "../../interfaces/items-selection-config";

export class ChipsSelectAutocompleteConfig<T> extends ItemsSelectionConfig<T> {
	override initialValue?: Array<T>;
	chipDisplayWith!: ItemDisplayFunction<T>;

	constructor(
		payload: BaseItemsSelectionConfig<T> & {
			chipDisplayWith: ItemDisplayFunction<T>;
		},
	) {
		super(payload);
		this.chipDisplayWith = payload.chipDisplayWith;
	}
}
