import { ItemsSelectionComponent } from "../../interfaces/items-selection-component.interface";
import { ChipsSelectAutocompleteConfig } from "./chips-select-autocomplete.config.type";
import { ItemsSelectionConfigProcessor } from "../../interfaces/items-selection-config";
import { ControlContainer, FormControl, FormGroup } from "@angular/forms";
import { MaterialImportsModule } from "../../material-imports.module";
import { CommonModule } from "@angular/common";
import { ENTER } from "@angular/cdk/keycodes";
import { Observable } from "rxjs";
import {
	AfterViewInit,
	ChangeDetectorRef,
	Component,
	ElementRef,
	EventEmitter,
	Input,
	Optional,
	Output,
	ViewChild,
} from "@angular/core";

@Component({
	selector: "td-chips-select-autocomplete",
	standalone: true,
	imports: [CommonModule, MaterialImportsModule],
	templateUrl: "./chips-select-autocomplete.component.html",
	styleUrls: ["./chips-select-autocomplete.component.scss"],
})
export class ChipsSelectAutocompleteComponent<T>
	implements ItemsSelectionComponent<T>, AfterViewInit
{
	@Input() config!: ChipsSelectAutocompleteConfig<T>;

	@Output() itemAdded = new EventEmitter<T>();
	@Output() itemRemoved = new EventEmitter<T>();

	separatorKeycodes = [ENTER];

	options: Array<T> = [];
	selectedItems: Array<T> = [];

	/* Autocomplete */
	@ViewChild("itemsInput") itemsInput!: ElementRef<HTMLInputElement>;
	inputControl = new FormControl<any>("");
	formGroup?: FormGroup;
	selectedItemsControl = new FormControl<T[]>([]);
	filteredOptions?: Observable<Array<T>>;

	constructor(
		@Optional() public readonly controlContainer: ControlContainer,
		private readonly changeRef: ChangeDetectorRef,
	) {}

	ngAfterViewInit() {
		this._configure();

		if (this.config.initialValue) {
			this.selectedItems = this.config.initialValue;
			this.selectedItemsControl.setValue(this.config.initialValue);
		}

		// overwrite target control with actual selected values
		if (this.config.formControlName && this.formGroup) {
			this.formGroup.setControl(
				this.config.formControlName,
				this.selectedItemsControl,
			);
		}

		this.changeRef.detectChanges();
	}

	ShouldDisplay(option: any): boolean {
		const hasOption = this.selectedItems.filter((item) =>
			this.config.compareWith!(option, item),
		);

		return hasOption.length === 0;
	}

	onRemove(evt: any): void {
		this.selectedItems = this.selectedItems?.filter(
			(item) => !this.config.compareWith!(item, evt),
		);
		this.selectedItemsControl.setValue(this.selectedItems);
		this.selectedItemsControl.markAsDirty();

		this.itemRemoved.emit(evt);
		this.changeRef.markForCheck();
	}

	itemSelected(evt: { option: { value?: any } }): void {
		if (!evt.option.value) return;

		this.selectedItems?.push(evt.option.value);
		this.selectedItemsControl.setValue(this.selectedItems);
		this.selectedItemsControl.markAsDirty();

		/* Clear dirty inputs */
		this.inputControl.setValue("");
		this.itemsInput.nativeElement.value = "";

		this.itemAdded.emit(evt.option.value);
		this.changeRef.markForCheck();
	}

	private _configure(): void {
		const p = new ItemsSelectionConfigProcessor(this);
		p.SetupFilteredOptions();
		p.FetchOptions();
		p.SetupFormControlState();
	}
}
