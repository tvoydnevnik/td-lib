import { ItemsSelectionConfig } from "../../interfaces/items-selection-config";

export class ItemSelectAutocompleteConfig<T> extends ItemsSelectionConfig<T> {
	override initialValue?: T;
}
