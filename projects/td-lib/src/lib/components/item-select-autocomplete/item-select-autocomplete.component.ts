import { ItemsSelectionComponent } from "../../interfaces/items-selection-component.interface";
import { ItemsSelectionConfigProcessor } from "../../interfaces/items-selection-config";
import { ItemSelectAutocompleteConfig } from "./item-select-autocomplete.config.type";
import { MaterialImportsModule } from "../../material-imports.module";
import { map, Observable, startWith } from "rxjs";
import { CommonModule } from "@angular/common";
import {
	ControlContainer,
	FormControl,
	FormGroup,
	Validators,
} from "@angular/forms";
import {
	AfterViewInit,
	ChangeDetectorRef,
	Component,
	EventEmitter,
	Input,
	Optional,
	Output,
} from "@angular/core";

@Component({
	selector: "td-item-select-autocomplete",
	standalone: true,
	imports: [CommonModule, MaterialImportsModule],
	templateUrl: "./item-select-autocomplete.component.html",
	styleUrls: ["./item-select-autocomplete.component.scss"],
})
export class ItemSelectAutocompleteComponent<T>
	implements ItemsSelectionComponent<T>, AfterViewInit
{
	@Input() config!: ItemSelectAutocompleteConfig<T>;
	@Output() itemChosen = new EventEmitter<T>();

	filteredOptions?: Observable<Array<T>>;
	formGroup?: FormGroup;
	inputControl = new FormControl<T | string>("", { nonNullable: true });
	actualValue = new FormControl<T | undefined>(undefined, {
		nonNullable: true,
	});

	options: Array<T> = [];

	constructor(
		@Optional() public readonly controlContainer: ControlContainer,
		private readonly changeRef: ChangeDetectorRef,
	) {}

	ngAfterViewInit() {
		this._configure();

		this.actualValue.statusChanges.subscribe((s) => {
			switch (s) {
				case "DISABLED":
				case "VALID":
					this.inputControl.setErrors(null);
					break;
				case "PENDING":
				case "INVALID":
					this.inputControl.setErrors(this.actualValue.errors);
					break;
			}
		});

		if (this.config.required)
			this.actualValue.setValidators(Validators.required);

		if (this.config.initialValue) {
			this.actualValue.setValue(this.config.initialValue);
			this.inputControl.setValue(this.config.initialValue);
		}

		if (this.config.formControlName && this.formGroup)
			this.formGroup.setControl(this.config.formControlName, this.actualValue);

		this.changeRef.detectChanges();
	}

	ItemSelected(evt: { option?: { value: any } }): void {
		this.actualValue.setValue(evt.option?.value);
		this.itemChosen.emit(evt.option?.value);
	}

	private _configure(): void {
		const p = new ItemsSelectionConfigProcessor(this);

		this.filteredOptions = this.inputControl.valueChanges.pipe(
			startWith(""),
			map((value) => {
				if (typeof value !== "string") return this.options;

				if (value !== "") this.actualValue.setValue(undefined);

				const filter = value || "";
				return filter
					? this.options.filter((o) =>
							this.config
								.displayWith(o)
								.toLowerCase()
								.includes(filter.toLowerCase()),
					  )
					: this.options;
			}),
		);
		p.FetchOptions();
		p.SetupFormControlState();
	}
}
