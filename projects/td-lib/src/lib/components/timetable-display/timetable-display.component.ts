import { DisplayableTimetable } from "../../interfaces/displayable-timetable";
import { AfterViewInit, Component, Input, OnChanges } from "@angular/core";
import { TimePipe } from "../../pipes/time.pipe";
import { CommonModule } from "@angular/common";
import { RouterLink } from "@angular/router";
import { Types } from "td-types";

export type TimetableColumns =
	| "index"
	| "time"
	| "form"
	| "lesson"
	| "unit"
	| "marks"
	| "homework";

@Component({
	selector: "td-timetable",
	standalone: true,
	imports: [CommonModule, TimePipe, RouterLink],
	templateUrl: "./timetable-display.component.html",
	styleUrls: ["./timetable-display.component.scss"],
})
export class TimetableDisplayComponent implements AfterViewInit, OnChanges {
	@Input() timetable!: Types.ITimetableResponse;

	/* Configuration inputs */
	@Input() displayColumns: TimetableColumns[] = [
		"index",
		"time",
		"form",
		"lesson",
		"unit",
	];
	@Input() displaySubgroups = true;
	@Input() displayTitle = true;
	@Input() formPrefix = false;
	@Input() baseFormLink: string | undefined;
	// @Input() highlightActive = false;

	displayable?: DisplayableTimetable;

	ngAfterViewInit() {
		this.displayable = new DisplayableTimetable(this.timetable);
	}

	ngOnChanges() {
		this.displayable = new DisplayableTimetable(this.timetable);
	}

	// CheckIsActive(
	// 	day_date: string,
	// 	start_time: string,
	// 	end_time: string,
	// ): boolean {
	// 	const now = DateTime.now();
	// 	const date = DateTime.fromISO(day_date);
	//
	// 	const is_same_day = now.hasSame(date, "day");
	// 	if (!is_same_day) return false;
	//
	// 	const start = DateTime.fromISO(start_time);
	// 	const end = DateTime.fromISO(end_time);
	//
	// 	return now >= start && now <= end;
	// }
}
