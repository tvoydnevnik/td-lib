import { ItemsSelectionComponent } from "./items-selection-component.interface";
import { isObservable, map, Observable, ReplaySubject, startWith } from "rxjs";
import { FormGroup, Validators } from "@angular/forms";
import {
	ItemCompareFunction,
	ItemDisplayFunction,
} from "./item-list-functions.type";

export type OptionsLike<T> = Array<T> | Observable<Array<T>>;
export type Options<T> = Array<T>;

export class BaseItemsSelectionConfig<T> {
	label!: string;
	options!: OptionsLike<T>;
	displayWith!: ItemDisplayFunction<T>;
	initialValue?: T | Array<T>;
	required? = false;
	disabled? = false;
	required_hint? = false;
	formControlName?: string;
	compareWith?: ItemCompareFunction<T> = (item, other) =>
		JSON.stringify(item) === JSON.stringify(other);

	constructor(payload: BaseItemsSelectionConfig<T>) {
		this.label = payload.label;
		this.options = payload.options;
		this.displayWith = payload.displayWith;

		this.initialValue = payload.initialValue;
		this.required = payload.required;
		this.disabled = payload.disabled;
		this.required_hint = payload.required_hint;
		this.formControlName = payload.formControlName;
		this.compareWith = payload.compareWith;
	}
}

export class ItemsSelectionConfig<T> extends BaseItemsSelectionConfig<T> {
	public options_changed = new ReplaySubject<void>(1);

	private readonly optionStream: ReplaySubject<Array<T>> = new ReplaySubject(1);
	private actual_options: Options<T> = [];

	constructor(payload: BaseItemsSelectionConfig<T>) {
		super(payload);
		this.SetOptions(payload.options);

		this.optionStream.subscribe({
			next: (v) => {
				this.actual_options = v;
				this.options_changed.next();
			},
			complete: () => {
				this.options_changed.complete();
			},
		});
	}

	GetOptions(): Options<T> {
		return this.actual_options;
	}

	SetOptions(options: OptionsLike<T>) {
		if (isObservable(options)) {
			options.subscribe(this.optionStream);
		} else {
			this.optionStream?.next(options);
		}
	}
}

export class ItemsSelectionConfigProcessor<T> {
	constructor(private readonly component: ItemsSelectionComponent<T>) {}

	SetupFilteredOptions(): void {
		this.component.filteredOptions =
			this.component.inputControl.valueChanges.pipe(
				startWith(""),
				map((value) => {
					if (typeof value !== "string") return this.component.options;

					const filter = value ?? "";
					return filter
						? this.component.options.filter((o) =>
								this.component.config
									.displayWith(o)
									.toLowerCase()
									.includes(filter.toLowerCase()),
						  )
						: this.component.options;
				}),
			);
	}

	FetchOptions(): void {
		this.component.config.options_changed.subscribe(() => {
			this.component.options = this.component.config.GetOptions();

			// emits `valueChanges` event and filters new options by existing filter value
			this.component.inputControl.setValue(this.component.inputControl.value);
		});
	}

	SetupFormControlState(): void {
		if (this.component.controlContainer) {
			this.component.formGroup = <FormGroup>(
				this.component.controlContainer.control
			);
		}

		if (this.component.config.required) {
			this.component.inputControl.addValidators(Validators.required);
			this.component.inputControl.updateValueAndValidity();
		}

		if (this.component.config.disabled) {
			this.component.inputControl.disable();
		}
	}
}
