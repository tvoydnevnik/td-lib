export type ItemDisplayFunction<T> = (item?: T) => string;
export type ItemCompareFunction<T> = (item?: T, other?: T) => boolean;
