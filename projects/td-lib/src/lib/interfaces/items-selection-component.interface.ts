import { ControlContainer, FormControl, FormGroup } from "@angular/forms";
import { ItemsSelectionConfig, Options } from "./items-selection-config";
import { Observable } from "rxjs";

export interface ItemsSelectionComponent<T> {
	config: ItemsSelectionConfig<T>;
	filteredOptions?: Observable<Array<T>>;
	formGroup?: FormGroup;
	inputControl: FormControl<T | string>;
	options: Options<T>;
	controlContainer: ControlContainer;
}
