import { DateTime } from "luxon";
import { Types } from "td-types";

type TimetableMap = Map<string, IndexGroup>;

export class DisplayableTimetable {
	data: TimetableMap;

	constructor(payload: Types.ITimetableResponse) {
		this.data = new Map();
		payload.timetable.forEach((entry) => {
			const weekStartDate = DateTime.fromISO(payload.start_date);
			const date =
				weekStartDate
					.plus({ days: entry.weekday - (weekStartDate.weekday - 1) })
					.toISODate() ?? payload.start_date;

			if (!this.data.has(date)) {
				this.data.set(date, new IndexGroup());
			}

			this.data.get(date)!.Push(entry);
		});
	}
}

class IndexGroup {
	private _data: Array<{
		index: number;
		start_time: string;
		end_time: string;
		timetable: Types.ITimetableEntry[];
	}> = [];

	Push(entry: Types.ITimetableEntry): void {
		let group = this._data.find((v) => v.index === entry.index);
		if (!group) {
			group = this._data.at(
				this._data.push({
					index: entry.index,
					start_time: entry.start_time ?? "",
					end_time: entry.end_time ?? "",
					timetable: [],
				}) - 1,
			);
		}

		group?.timetable.push(entry);
	}

	get Data() {
		return this._data;
	}
}
