import { TdIfDirective } from "../../../directives/td-if.directive";
import { CommonModule } from "@angular/common";
import { Component } from "@angular/core";

@Component({
	selector: "test-td-if",
	standalone: true,
	imports: [CommonModule, TdIfDirective],
	templateUrl: "./td-if.component.html",
	styleUrls: ["./td-if.component.scss"],
})
export class TdIfComponent {}
