import { TdIfComponent } from "../test/components/td-if/td-if.component";
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { TdIfDirective } from "./td-if.directive";

describe("TdIf directive", function () {
	let component: ComponentFixture<TdIfComponent>;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [TdIfDirective, TdIfComponent],
			schemas: [CUSTOM_ELEMENTS_SCHEMA],
		});

		component = TestBed.createComponent(TdIfComponent);
		component.detectChanges();
	});

	it("component should be created", function () {
		expect(component).toBeDefined();
	});

	it("should display element if regular value is truthy", function () {
		const truthyEl = component.nativeElement.querySelector("#true");
		expect(truthyEl).toBeTruthy();
	});

	it("should not display element if regular value is falsy", function () {
		const falsyEl = component.nativeElement.querySelector("#false");
		expect(falsyEl).toBeNull();
	});

	it("should display element if array value is not empty", function () {
		const arrayEl = component.nativeElement.querySelector("#array");
		expect(arrayEl).toBeTruthy();
	});

	it("should not display element if array value if empty", function () {
		const emptyArrayEl = component.nativeElement.querySelector("#empty-array");
		expect(emptyArrayEl).toBeNull();
	});
});
