import { Directive, Input, TemplateRef, ViewContainerRef } from "@angular/core";

export class TdIfContext<T> {
	$implicit: T;
	tdIf: T;
	result: T;

	constructor(value: T) {
		this.$implicit = value;
		this.tdIf = this.$implicit;
		this.result = this.$implicit;
	}
}

@Directive({
	standalone: true,
	selector: "[tdIf]",
})
export class TdIfDirective<T> {
	constructor(
		private readonly templateRef: TemplateRef<TdIfContext<T>>,
		private readonly vcRef: ViewContainerRef,
	) {}

	@Input()
	set tdIf(value: T) {
		const isTrue = this._checkExpression(value);

		this.vcRef.clear();
		if (isTrue)
			this.vcRef.createEmbeddedView(
				this.templateRef,
				new TdIfContext<T>(value),
			);
	}

	private _checkExpression(expr: T): boolean {
		if (Array.isArray(expr)) return expr.length !== 0;

		return !!expr;
	}
}
