import { Directive, Input, TemplateRef, ViewContainerRef } from "@angular/core";
import { OverlayService } from "../services/overlay.service";
import { LoadingComponent } from "../components/loading/loading.component";

export class TdLoadContext<T> {
	$implicit: T;
	tdLoad: T;
	result: T;

	constructor(value: T) {
		this.$implicit = value;
		this.tdLoad = this.$implicit;
		this.result = this.$implicit;
	}
}

@Directive({
	standalone: true,
	selector: "[tdLoad]",
})
export class TdLoadDirective<T> {
	constructor(
		private readonly overlayService: OverlayService,
		private readonly templateRef: TemplateRef<TdLoadContext<T>>,
		private readonly vcRef: ViewContainerRef,
	) {}

	@Input()
	set tdLoad(value: T) {
		const isTrue = this._checkExpression(value);

		this.vcRef.clear();
		if (!isTrue) {
			this.overlayService.createOverlay(LoadingComponent);
		} else {
			this.overlayService.clearOverlay();
			this.vcRef.createEmbeddedView(
				this.templateRef,
				new TdLoadContext<T>(value),
			);
		}
	}

	private _checkExpression(expr: T): boolean {
		return !!expr;
	}
}
