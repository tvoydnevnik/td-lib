import { NgModule } from "@angular/core";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { ReactiveFormsModule } from "@angular/forms";
import { MatChipsModule } from "@angular/material/chips";
import { MatIconModule } from "@angular/material/icon";
import { MatTooltipModule } from "@angular/material/tooltip";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";

const MATERIAL_COMPONENTS = [
	MatPaginatorModule,
	MatExpansionModule,
	MatAutocompleteModule,
	MatFormFieldModule,
	MatInputModule,
	MatChipsModule,
	MatIconModule,
	ReactiveFormsModule,
	MatTooltipModule,
	MatSlideToggleModule,
];

@NgModule({
	imports: [...MATERIAL_COMPONENTS],
	exports: [...MATERIAL_COMPONENTS],
})
export class MaterialImportsModule {}
