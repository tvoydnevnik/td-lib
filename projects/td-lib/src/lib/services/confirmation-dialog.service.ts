import { ConfirmationDialogComponent } from "../components/confirmation-dialog/confirmation-dialog.component";
import { MatDialog } from "@angular/material/dialog";
import { Injectable } from "@angular/core";
import { filter, Observable } from "rxjs";

const DEFAULT_WARNING =
	"This action is irreversible. Once it is completed, it can not be undone.";

@Injectable({
	providedIn: "root",
})
export class ConfirmationDialogService {
	constructor(private readonly dialog: MatDialog) {}

	Open(
		title: string,
		warning = DEFAULT_WARNING,
	): Observable<{ confirmed: boolean }> {
		const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
			data: { title, warning },
		});

		return dialogRef.afterClosed().pipe(filter((v: any) => v?.confirmed));
	}
}
