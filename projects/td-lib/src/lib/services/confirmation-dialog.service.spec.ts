import { ConfirmationDialogService } from "./confirmation-dialog.service";
import { MatDialog } from "@angular/material/dialog";
import { TestBed } from "@angular/core/testing";
import { cold } from "jasmine-marbles";

describe("ConfirmationDialogService", () => {
	let service: ConfirmationDialogService;
	const matDialog = {
		open: jasmine.createSpy(),
	};
	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [],
			providers: [
				ConfirmationDialogService,
				{ provide: MatDialog, useValue: matDialog },
			],
		});
		service = TestBed.inject(ConfirmationDialogService);
	});

	it("should be created", function () {
		expect(service).toBeDefined();
	});

	it("should emit observable only when user confirms dialog", function () {
		const stream = cold("---a--b----c", {
			a: { confirmed: false }, // user closed dialog with button
			b: undefined, // user dismissed dialog any other way
			c: { confirmed: true }, // user accepted the dialog
		});
		const expected = cold("-----------c", { c: { confirmed: true } });

		matDialog.open.and.returnValue({
			afterClosed: jasmine.createSpy().and.returnValue(stream),
		});
		expect(service.Open("")).toBeObservable(expected);
	});
});
