import { ComponentRef, Injectable } from "@angular/core";
import { ComponentType, Overlay, OverlayRef } from "@angular/cdk/overlay";
import { ComponentPortal } from "@angular/cdk/portal";

@Injectable({
	providedIn: "root",
})
export class OverlayService {
	private _overlayRef: OverlayRef;

	private _component!: ComponentRef<any>;
	private _portal!: ComponentPortal<any>;

	constructor(private readonly overlay: Overlay) {
		this._overlayRef = overlay.create();
	}

	createOverlay(component: ComponentType<any>): void {
		if (this._overlayRef.hasAttached()) return;

		this._portal = new ComponentPortal(component);
		this._component = this._overlayRef.attach(this._portal);
	}

	clearOverlay(): void {
		this._overlayRef.detach();
	}
}
